from __future__ import print_function
from django.http.response import HttpResponse
from music.models import Album, Song
import json
# from trace_tools.decorators import trace


# @trace()
def album_json(request):
    if request.method == 'GET':
        album_id = request.GET.get('id')
        data = Album.objects.get(pk=album_id).to_json()
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    return HttpResponse(status=405)


def song_json(request):
    if request.method == 'GET':
        song_id = request.GET.get('id')
        data = Song.objects.get(pk=song_id).to_json()
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    return HttpResponse(status=405)
