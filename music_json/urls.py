from django.conf.urls import url
from . import views

app_name = 'music_json'

urlpatterns = [
    # /music_json/
    url(r'^album/$', views.album_json, name='album_json'),

    url(r'^song/$', views.song_json, name='song_json'),
]
