from __future__ import unicode_literals

from django.db import models


class Venue(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class FinalQuote(models.Model):
    venue = models.ForeignKey(Venue)
    charge = models.IntegerField(default=0)

    def __str__(self):
        return str(self.venue) + ":" + str(self.charge)
