from __future__ import unicode_literals

from django.db import models


class AbstractGeneralAttribute(models.Model):
    active = models.BooleanField(default=True)

    def to_json(self):
        json = {'active': self.active}
        return json

    class Meta:
        abstract = True


class CustomerSupportNumber(AbstractGeneralAttribute):
    number = models.CharField(max_length=50)

    def to_json(self):
        json = super(CustomerSupportNumber, self).to_json()
        json['number'] = self.number
        return json
