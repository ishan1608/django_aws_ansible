from django.http import HttpResponse
from models import CustomerSupportNumber
import json
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def contacts(request):
    if request.method == 'GET':
        numbers = CustomerSupportNumber.objects.filter(active=True)
        print(numbers)
        data = {'numbers': []}
        for number in numbers:
            data['numbers'].append(number.to_json())
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    elif request.method == 'POST':
        post_body = request.POST
        new_number = post_body.get('number')
        customer_support_number, created = CustomerSupportNumber.objects.get_or_create(
            number=new_number)
        data = {}
        data.update(customer_support_number.to_json())
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    return HttpResponse(status=405)
