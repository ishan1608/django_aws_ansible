from django.conf.urls import url
from . import views

app_name = 'customer_support'

urlpatterns = [
    # /customer_support/
    url(r'^contacts/$', views.contacts, name='contacts'),
]
