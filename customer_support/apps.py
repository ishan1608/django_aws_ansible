from __future__ import unicode_literals

from django.apps import AppConfig


class CustomerSupportConfig(AppConfig):
    name = 'customer_support'
