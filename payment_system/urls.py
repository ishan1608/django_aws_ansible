from django.conf.urls import url
from . import views

app_name = 'payment_system'

urlpatterns = [
    # /payment_system/
    url(r'^orders/$', views.orders, name='orders'),
    url(r'^payment_webhook/$', views.payment_webhook, name='payment_webhook'),
    url(r'^payment_redirect/$', views.payment_redirect, name='payment_redirect'),
]
