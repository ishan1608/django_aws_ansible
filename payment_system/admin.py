from django.contrib import admin
from models import Product, Order

admin.site.register(Product)


class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ('order_id', 'payment_url', 'paid',)
admin.site.register(Order, OrderAdmin)
