from __future__ import unicode_literals
from django.db import models
from utls import get_payment_api, get_redirect_url, get_webhook_url


class Product(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False, default="Nothing")
    price = models.IntegerField(default=0)

    def to_json(self):
        json = {
            'name': self.name,
            'price': self.price
        }
        return json

    def __str__(self):
        return self.name


class Order(models.Model):
    product = models.ForeignKey(Product)
    order_id = models.CharField(max_length=128, default='O')
    paid = models.BooleanField()
    payment_url = models.CharField(max_length=2048, default='')

    def create_order_id(self):
        self.order_id = str('Order' + str(self.id)).lower()
        self.save()
        return self.order_id

    def get_order_id(self):
        if self.order_id == 'O':
            return self.create_order_id()
        else:
            return self.order_id

    def create_payment_url(self):
        payment_api = get_payment_api()
        # print 'link creation'
        link_creation_data = payment_api.link_create(
            title=self.get_order_id(),
            description=self.product.name,
            base_price=self.product.price, webhook_url=get_webhook_url(),
            redirect_url=get_redirect_url()
        )
        # print link_creation_data
        self.payment_url = link_creation_data['link']['url']
        self.save()
        return self.payment_url
        # return link_creation_data

    def get_payment_url(self):
        if self.payment_url == '':
            return self.create_payment_url()
        else:
            return self.payment_url

    def to_json(self):
        json = {
            'product': self.product.to_json(),
            'order_id': self.get_order_id(),
            'paid': self.paid,
            'payment_url': self.get_payment_url(),
        }
        return json

    def __str__(self):
        return str(self.order_id) + str(self.product)
