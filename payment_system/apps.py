from __future__ import unicode_literals

from django.apps import AppConfig


class PaymentSystemConfig(AppConfig):
    name = 'payment_system'
