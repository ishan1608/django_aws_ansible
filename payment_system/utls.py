from django.core.urlresolvers import reverse
from instamojo import Instamojo
from django.conf import settings


def get_payment_api():
    if settings.DEBUG:
        # TEST server
        return Instamojo(
            api_key='39065e479c1feb4745db559a160317d4',
            auth_token='10755a40a44a16959e0a6e6d0dd60a7e',
            endpoint='https://test.instamojo.com/api/1.1/',
        )
    else:
        return Instamojo(
            api_key='367a49eecaee0b3f09eba645fd443030',
            auth_token='3e477472d5ac75f798e298f81532aa7e'
        )

base = 'http://ishan1608.tk'


def get_webhook_url():
    return base + reverse(viewname='payment_system:payment_webhook')


def get_redirect_url():
    return base + reverse(viewname='payment_system:payment_redirect')
