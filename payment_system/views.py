from django.http import HttpResponse
from models import Order, Product
import json
from django.views.decorators.csrf import csrf_exempt
from utls import get_payment_api


@csrf_exempt
def orders(request):
    if request.method == 'GET':
        get_params = request.GET
        pk = get_params.get('id')
        if pk:
            all_orders = Order.objects.filter(id=pk)
        else:
            # All orders
            all_orders = Order.objects.all()
        data = {
            'orders': []
        }
        for order in all_orders:
            data['orders'].append(order.to_json())
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    elif request.method == 'POST':
        post_params = request.POST
        product_id = post_params.get('product_id')
        product = Product.objects.get(pk=product_id)
        order = Order()
        order.product = product
        order.paid = False
        order.save()
        # Creating order_id
        # TODO It would be better if order_id initialization is done from the model itself
        # by overriding save function
        order.create_order_id()
        data = order.to_json()
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)
    else:
        return HttpResponse(status=405)


@csrf_exempt
def payment_webhook(request):
    payment_api = get_payment_api()
    if request.method == 'POST':
        try:
            post_params = request.POST
            payment_id = post_params.get('payment_id')
            payment = payment_api.payment_detail(payment_id)['payment']
            print 'payment details', payment
            # Appropriately handling successful payments for different types
            payment_slug = str(payment.get('link_slug')).lower()
            order = Order.objects.get(order_id=payment_slug)
            if str(payment.get('status')).lower() == 'credit':
                print 'Payment was credited'
                order.paid = True
            else:
                order.paid = False
                print 'Payment failed'
            order.save()
        except Exception as e:
            print str(e)
        return HttpResponse(json.dumps(payment), content_type='application/json', status=200)
    return HttpResponse(status=405)


@csrf_exempt
def payment_redirect(request):
    return HttpResponse('payment redirect', content_type='text/plain', status=200)
