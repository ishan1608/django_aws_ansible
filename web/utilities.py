from time import strftime

from django.utils.text import slugify
from settings import DEBUG


def get_media_path(instance, filename):
    """'
    Provides a path to be used to save/upload a file.
    It just takes model name, object, file name and appends time to it.
    """
    filename = '.'.join(map(slugify, filename.split('.')))
    self = instance
    if DEBUG:
        path_folders = "debug"
    else:
        path_folders = ""
    path_folders += "/%s/%s" % (
        self._meta.app_label.lower(), self._meta.object_name.lower())
    path_folders += strftime("/%Y/%m/%d/")
    return path_folders + filename
