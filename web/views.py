from django.http import HttpResponse

from ratelimit.decorators import ratelimit


@ratelimit(key='ip', rate='30/h', method=['GET', 'POST'], block=True)
def index(request):
    return HttpResponse('Hello')
