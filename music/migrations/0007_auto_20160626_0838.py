# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-26 08:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import sorl.thumbnail.fields
import web.utilities


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0006_auto_20160626_0823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='album',
            name='album_logo',
            field=sorl.thumbnail.fields.ImageField(max_length=255, upload_to=web.utilities.get_media_path),
        ),
        migrations.AlterField(
            model_name='song',
            name='album',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='music.Album'),
        ),
    ]
