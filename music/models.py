from __future__ import print_function
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from web.utilities import get_media_path
from sorl.thumbnail import get_thumbnail

import datetime


class Album(models.Model):
    artist = models.CharField(max_length=250)
    album_title = models.CharField(max_length=500)
    genre = models.CharField(max_length=100)
    album_logo = models.ImageField(upload_to=get_media_path, max_length=255)

    def get_absolute_url(self):
        return reverse('music:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.album_title + " : " + self.artist

    def get_songs(self):
        return self.song_set.all()

    def get_different_sized_logo(self):
        """
        Generates different sized logos of the album
        :return: json containing the different sized images
        """
        # print('get_different_sized_logo')
        sizes = [50, 100, 150]
        sized_logos = {}
        for size in sizes:
            # print(size)
            sized_logo = get_thumbnail(self.album_logo, str(size), quality=99)
            # print(sized_logo.url)
            sized_logos['logo_' + str(size)] = sized_logo.url
        # print(sized_logos)
        return sized_logos

    def to_json(self):
        json = {'id': self.id, 'artist': self.artist, 'title': self.album_title,
                'genre': self.genre, 'logo': self.album_logo.url}
        songs = []
        for song in self.get_songs():
            songs.append(song.to_json())
        json['songs'] = songs
        # Getting different sized logos
        print('getting logos')
        print(datetime.datetime.now().time())
        json.update(self.get_different_sized_logo())
        print(datetime.datetime.now().time())
        print('done getting logos')
        return json


class Song(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    file_type = models.CharField(max_length=10)
    song_title = models.CharField(max_length=250)
    is_favourite = models.BooleanField(default=False)

    def __str__(self):
        return self.song_title

    def to_json(self):
        json = {'id': self.id, 'title': self.song_title}
        return json
